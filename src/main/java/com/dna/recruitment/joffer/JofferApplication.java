package com.dna.recruitment.joffer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JofferApplication {

	public static void main(String[] args) {
		SpringApplication.run(JofferApplication.class, args);
	}

}
